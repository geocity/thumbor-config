# thumbor-service for Geocity


## Getting started

A simple setup for https://www.thumbor.org/ open-source smart on-demand image cropping, resizing and filters that is used by Geocity Agenda module in order to get device adapted image formats and resolutions


## Installation

Clone this repository on you destination server 

Adapt the name of the geocity_network accordingly in docker-compose.yml with the network name defined in Geocity.

and finally ```docker-compose up```


Important: Geocity service must be up already in order to allow thumbor composition to find geocity_network


## License
AGPLv3

